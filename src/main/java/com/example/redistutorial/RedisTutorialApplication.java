package com.example.redistutorial;

import com.example.redistutorial.adapter.out.message_broker.ESBFulfillment;
import com.example.redistutorial.adapter.helper.OtherConfiguration;
import com.example.redistutorial.domain.util.Util;
import com.example.redistutorial.adapter.out.message_broker.RabbitMQConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.google.gson.JsonObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@SpringBootApplication
public class RedisTutorialApplication implements CommandLineRunner {
	@Autowired
	RabbitMQConfig rabbitMQProducer;


	@Autowired
	OtherConfiguration otherConfiguration;

	public static void main(String[] args) {
		SpringApplication.run(RedisTutorialApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		for (int i = 0; i<otherConfiguration.getIncentiveRulesTimeout(); i++){
			String lastIteration= Integer.toString(i).substring(Integer.toString(i).length()-1);
			JsonObject mainTrigger = new JsonObject();
			mainTrigger.addProperty("main_trigger","{\"business_id\":\"00054648\"}");
			Map<String,ArrayList> bonusMap = new HashMap<>();
			ArrayList<String> bonusArray = new ArrayList<>();
			bonusArray.add("00054648");
			bonusMap.put("business_id",bonusArray);
			JsonObject bonus = new JsonObject();
			bonus.addProperty("business_id","['00054648']");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			ESBFulfillment esbFulfillment = new ESBFulfillment(
					Util.generateID(),
					"628120000"+lastIteration,
					mainTrigger,
					new Util().mapToJson(bonusMap),
					otherConfiguration.getIncentiveState(),
					"INCTR_"+i,
					"INCTR"+i,
					otherConfiguration.getChannel(),
					0,
					LocalDateTime.now().format(formatter).toString()
			);
			rabbitMQProducer.sendMessage(esbFulfillment,otherConfiguration.getQueueName()+lastIteration);
		}

		Util.tdrLogger.info("");
	}


}
