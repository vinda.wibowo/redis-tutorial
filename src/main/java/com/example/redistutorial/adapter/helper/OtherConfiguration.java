package com.example.redistutorial.adapter.helper;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class OtherConfiguration {
    @Value("${incentive.rules.timeout}")
    private int incentiveRulesTimeout;

    @Value("${incentive_rules.incentiveState}")
    private String incentiveState;

    @Value("${incentive_rules.channel}")
    private String channel;

    @Value("${rabbitmq.queue.name}")
    private String queueName;

}
