package com.example.redistutorial.adapter.out.message_broker;

import com.example.redistutorial.domain.util.Util;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.charset.StandardCharsets;

@Configuration
public class RabbitMQConfig {
    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.routing.key}")
    private String routingKey;

    @Value("${rabbitmq.queue.name0}")
    private String queue0;
    @Value("${rabbitmq.queue.name1}")
    private String queue1;
    @Value("${rabbitmq.queue.name2}")
    private String queue2;
    @Value("${rabbitmq.queue.name3}")
    private String queue3;
    @Value("${rabbitmq.queue.name4}")
    private String queue4;
    @Value("${rabbitmq.queue.name5}")
    private String queue5;
    @Value("${rabbitmq.queue.name6}")
    private String queue6;
    @Value("${rabbitmq.queue.name7}")
    private String queue7;
    @Value("${rabbitmq.queue.name8}")
    private String queue8;
    @Value("${rabbitmq.queue.name9}")
    private String queue9;


    // spring bean for rabbitmq queue
    @Bean
    public Queue queue0(){
        return new Queue(queue0);
    }

    @Bean
    public Queue queue1(){
        return new Queue(queue1);
    }

    @Bean
    public Queue queue2(){
        return new Queue(queue2);
    }

    @Bean
    public Queue queue3(){
        return new Queue(queue3);
    }

    @Bean
    public Queue queue4(){
        return new Queue(queue4);
    }

    @Bean
    public Queue queue5(){
        return new Queue(queue5);
    }

    @Bean
    public Queue queue6(){
        return new Queue(queue6);
    }

    @Bean
    public Queue queue7(){
        return new Queue(queue7);
    }

    @Bean
    public Queue queue8(){
        return new Queue(queue8);
    }

    @Bean
    public Queue queue9(){
        return new Queue(queue9);
    }

    private RabbitTemplate rabbitTemplate;

    public RabbitMQConfig(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(ESBFulfillment esbFulfillment, String queue){
        Util.debugLogger.debug("Message sent -> {} to Queue: {}", esbFulfillment,queue);
        if (isQueueExist(queue)){
            String message=new Util().esbFulfillmentToJSONObject(esbFulfillment).toString();
            rabbitTemplate.convertAndSend(queue,message.getBytes(StandardCharsets.UTF_8));
            rabbitTemplate.destroy();
        }

    }

    private boolean isQueueExist(String queue){
        switch (queue) {
            case "queue_fulfillment0":
                return true;
            case "queue_fulfillment1":
                return true;
            case "queue_fulfillment2":
                return true;
            case "queue_fulfillment3":
                return true;
            case "queue_fulfillment4":
                return true;
            case "queue_fulfillment5":
                return true;
            case "queue_fulfillment6":
                return true;
            case "queue_fulfillment7":
                return true;
            case "queue_fulfillment8":
                return true;
            case "queue_fulfillment9":
                return true;
            default: return false;
        }
    }
}